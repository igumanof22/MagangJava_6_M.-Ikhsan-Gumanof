-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: sdm
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `departemen`
--

DROP TABLE IF EXISTS `departemen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departemen` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `departemen`
--

LOCK TABLES `departemen` WRITE;
/*!40000 ALTER TABLE `departemen` DISABLE KEYS */;
INSERT INTO `departemen` VALUES (1,'Manajemen'),(2,'Pengembangan Bisnis'),(3,'Teknisi'),(4,'Analis');
/*!40000 ALTER TABLE `departemen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `karyawan`
--

DROP TABLE IF EXISTS `karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) NOT NULL,
  `jenis kelamin` enum('L','P') NOT NULL,
  `status` enum('Menikah','Belum') NOT NULL,
  `tanggal lahir` date NOT NULL,
  `tanggal masuk` date NOT NULL,
  `Departemen` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Departemen` (`Departemen`),
  CONSTRAINT `karyawan_ibfk_1` FOREIGN KEY (`Departemen`) REFERENCES `departemen` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `karyawan`
--

LOCK TABLES `karyawan` WRITE;
/*!40000 ALTER TABLE `karyawan` DISABLE KEYS */;
INSERT INTO `karyawan` VALUES (1,'Rizki Saputra','L','Menikah','1980-10-11','2011-01-01',1),(2,'Farhan Reza','L','Belum','1989-11-01','2011-01-01',1),(3,'Riyando Adi','L','Menikah','1977-01-25','2011-01-01',1),(4,'Diego Manuel','L','Menikah','1983-02-22','2012-09-04',2),(5,'Satya Laksana','L','Menikah','1981-01-12','2011-03-19',2),(6,'Miguel Hernandez','L','Menikah','1994-10-16','2014-06-15',2),(7,'Putri Persada','P','Menikah','1988-01-30','2013-04-14',2),(8,'Alma Safira','P','Menikah','1991-05-18','2013-09-28',3),(9,'Haqi Hafiz','L','Belum','1995-09-19','2015-03-09',3),(10,'Abi Isyawara','L','Belum','1991-06-03','2012-01-22',3),(11,'Maman Kresna','L','Belum','1993-08-21','2012-09-15',3),(12,'Nadia Aulia','P','Belum','1989-09-07','2012-05-07',4),(13,'Mutiara Rezki','P','Menikah','1988-03-23','2013-05-21',4),(14,'Dani Setiawan','L','Belum','1986-02-11','2014-11-30',4),(15,'Budi Putra','L','Belum','1995-10-23','2015-12-03',4);
/*!40000 ALTER TABLE `karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'sdm'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-11 22:55:05
