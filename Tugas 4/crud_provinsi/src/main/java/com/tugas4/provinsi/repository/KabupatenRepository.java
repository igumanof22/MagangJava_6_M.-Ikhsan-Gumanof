package com.tugas4.provinsi.repository;

import com.tugas4.provinsi.model.KabupatenKota;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KabupatenRepository extends JpaRepository<KabupatenKota, Integer> {
}
