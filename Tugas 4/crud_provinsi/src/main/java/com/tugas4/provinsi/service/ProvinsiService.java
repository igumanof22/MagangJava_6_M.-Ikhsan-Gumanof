package com.tugas4.provinsi.service;

import com.tugas4.provinsi.model.Provinsi;
import com.tugas4.provinsi.repository.ProvinsiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ProvinsiService {
    @Autowired
    ProvinsiRepository repo;
    @Autowired
    JdbcTemplate jdbc;

    public List<Provinsi> getAllProvinsi(){
        return repo.findAll();
    }

    public List<Provinsi> getAllDaerah(){
        List<Provinsi> listProvinsi = jdbc.query(
                "SELECT p.id, p.nama, kab.id, kab.nama, kec.id, kec.nama , d.nama FROM " +
                        "provinsi p LEFT JOIN kabupaten_kota kab ON p.id=kab.provinsi_id  " +
                        "LEFT JOIN kecamatan kec ON kab.id=kec.kabupaten_id " +
                        "LEFT JOIN desa d ON kec.id=d.kecamatan_id order by p.id, d.id" ,
                (rs, rowNum) -> new Provinsi(rs.getInt("p.id"), rs.getString("p.nama"),
                        rs.getString("kab.nama"),
                        rs.getString("kec.nama"),rs.getString("d.nama")));
        return listProvinsi;
    }

    public void save(Provinsi provinsi){
        repo.save(provinsi);
    }

    public Provinsi getProvinsi(int id){
        return repo.findById(id).get();
    }

    public void delete(int id){
        repo.deleteById(id);
    }


}
