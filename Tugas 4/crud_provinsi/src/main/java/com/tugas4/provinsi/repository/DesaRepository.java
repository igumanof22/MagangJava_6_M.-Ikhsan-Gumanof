package com.tugas4.provinsi.repository;

import com.tugas4.provinsi.model.Desa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DesaRepository extends JpaRepository<Desa, Integer> {
}
