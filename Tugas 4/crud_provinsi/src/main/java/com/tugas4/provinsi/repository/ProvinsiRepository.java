package com.tugas4.provinsi.repository;

import com.tugas4.provinsi.model.Provinsi;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProvinsiRepository extends JpaRepository<Provinsi, Integer> {
//    @Query("Select p.nama, k.nama from provinsi p JOIN kabupaten_kota k")
//    public List<Provinsi> listProvinsi();
}
