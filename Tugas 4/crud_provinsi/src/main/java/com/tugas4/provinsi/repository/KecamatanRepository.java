package com.tugas4.provinsi.repository;

import com.tugas4.provinsi.model.Kecamatan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KecamatanRepository extends JpaRepository<Kecamatan, Integer> {

}
