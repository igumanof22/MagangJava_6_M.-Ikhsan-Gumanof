package com.tugas4.aplikasi_struktur_orgasnisasi.service;

import com.tugas4.aplikasi_struktur_orgasnisasi.model.company;
import com.tugas4.aplikasi_struktur_orgasnisasi.repository.companyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class companyService {
    @Autowired
    private companyRepository comRepo;

    public List<company> listAll(){
        return comRepo.findAll();
    }

    public void save(company company){
        comRepo.save(company);
    }

    public company get(int id){
        return comRepo.findById(id).get();
    }

    public void delete(int id){
        comRepo.deleteById(id);
    }
}
