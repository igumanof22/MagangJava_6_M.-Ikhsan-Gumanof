package com.tugas4.aplikasi_struktur_orgasnisasi.repository;

import com.tugas4.aplikasi_struktur_orgasnisasi.model.company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface companyRepository extends JpaRepository<company, Integer> {
}
