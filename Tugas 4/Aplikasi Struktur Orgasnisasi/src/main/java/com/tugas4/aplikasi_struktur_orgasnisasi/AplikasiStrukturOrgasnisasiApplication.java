package com.tugas4.aplikasi_struktur_orgasnisasi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AplikasiStrukturOrgasnisasiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AplikasiStrukturOrgasnisasiApplication.class, args);
    }

}
