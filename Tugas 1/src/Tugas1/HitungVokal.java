package Tugas1;

import java.util.Objects;
import java.util.Scanner;

import static java.lang.System.*;

public class HitungVokal {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        out.print("Kalimat : ");
        String kalimat = in.nextLine();

        out.println("-----------------");

        String[] str;
        str = vocal(kalimat);

        out.println('"'+kalimat+'"'+" = "+str[1]+" yaitu "+str[0]);
    }

    private static String[] vocal(String kalimat){
        int x = 0;
        boolean cekA = false;
        boolean cekI = false;
        boolean cekU = false;
        boolean cekE = false;
        boolean cekO = false;
        String[] str = new String[5];
        String[] output = new String[2];
        StringBuilder out = null;

        for(int i=0; i<kalimat.length(); i++){
            if(!cekA){
                if(kalimat.charAt(i)=='a'||kalimat.charAt(i)=='A'){
                    str[x] = "a";
                    x++;
                    cekA=true;
                }
            }
            if(!cekI){
                if(kalimat.charAt(i)=='i'||kalimat.charAt(i)=='I'){
                    str[x] = "i";
                    x++;
                    cekI=true;
                }
            }
            if(!cekU){
                if(kalimat.charAt(i)=='u'||kalimat.charAt(i)=='U'){
                    str[x] = "u";
                    x++;
                    cekU=true;
                }
            }
            if(!cekE){
                if(kalimat.charAt(i)=='e'||kalimat.charAt(i)=='E'){
                    str[x] = "e";
                    x++;
                    cekE=true;
                }
            }
            if(!cekO){
                if(kalimat.charAt(i)=='o'||kalimat.charAt(i)=='O'){
                    str[x] = "o";
                    x++;
                    cekO=true;
                }
            }
        }

        for(int i=0; i<str.length; i++){
            if(str[i]!=null){
                if(i==0){
                    out = new StringBuilder(str[i]);
                }else{
                    Objects.requireNonNull(out).append(", ").append(str[i]);
                }
            }
        }

        output[0] = Objects.requireNonNull(out).toString();
        output[1] = String.valueOf(x);
        return output;
    }
}
