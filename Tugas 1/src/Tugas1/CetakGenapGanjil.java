package Tugas1;

import java.util.Scanner;

import static java.lang.System.*;

public class CetakGenapGanjil {
    public static void main(String[] args) {
        int bil1;
        int bil2;

        Scanner bil = new Scanner(in);

        out.print("Bilangan A : ");
        bil1 = bil.nextInt();
        out.print("Bilangan B : ");
        bil2 = bil.nextInt();

        out.println(genapganjil(bil1, bil2));
    }

    public static String genapganjil(int a, int b){
        out.println("-----------------");
        for(int i=a; i<=b; i++){
            if(i%2==0){
                out.println("Angka "+i+" adalah genap");
            }else{
                out.println("Angka "+i+" adalah ganjil");
            }
        }
        return "----------------";
    }
}