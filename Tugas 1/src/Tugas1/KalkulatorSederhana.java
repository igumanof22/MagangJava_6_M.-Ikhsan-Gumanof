package Tugas1;

import java.util.Scanner;

import static java.lang.System.*;

public class KalkulatorSederhana {
    private static String kalkulator(String str){
        String[] split = str.split(" ");
        double hasil;
        String out;

        switch (split[1]) {
            case "+":
                hasil = Double.parseDouble(split[0]) + Double.parseDouble(split[2]);
                out = String.valueOf(hasil);
                break;
            case "-":
                hasil = Double.parseDouble(split[0]) - Double.parseDouble(split[2]);
                out = String.valueOf(hasil);
                break;
            case "/":
                if(split[2].equals("0")){
                    out = "Tidak bisa dilakukan";
                } else{
                    hasil = Double.parseDouble(split[0]) / Double.parseDouble(split[2]);
                    out = String.valueOf(hasil);
                }   break;
            case "x":
                hasil = Double.parseDouble(split[0]) * Double.parseDouble(split[2]);
                out = String.valueOf(hasil);
                break;
            default:
                out = "Format tidak sesuai";
                break;
        }

        return out;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        out.println("+ - / x");
        out.print("Input (ex. 2 + 2) : ");
        String str = in.nextLine();

        out.println("--------------");
        out.println(kalkulator(str));
    }
}
