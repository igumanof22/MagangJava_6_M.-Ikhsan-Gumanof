package com.tugas4.aplikasi_struktur_orgasnisasi.service;

import com.tugas4.aplikasi_struktur_orgasnisasi.model.employee;
import com.tugas4.aplikasi_struktur_orgasnisasi.repository.employeeRepository;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class employeeService {
    @Autowired
    private employeeRepository empRepo;
    @Autowired
    JdbcTemplate jdbc;


    public List<employee> listAll(){
        return empRepo.findAll();
    }
    public List<employee> listTampil(){
        List<employee> listEmployee = jdbc.query(
                "SELECT e.id, e.nama, if(em.nama=e.nama,'CEO', if(em.id='1','Direktur', " +
                        "if(em.id='2'||em.id='3','Manager', 'Staff'))) as posisi, c.nama " +
                        "from employee e join employee em join company c on e.company_id = c.id " +
                        "where e.atasan_id = em.id or e.atasan_id is null group by e.id" ,
                (rs, rowNum) -> new employee(rs.getInt("e.id"), rs.getString("e.nama"),
                        rs.getString("posisi"),rs.getString("c.nama")));
        return listEmployee;
    }

    public void save(employee employee){
        empRepo.save(employee);
    }

    public employee get(int id){
        return empRepo.findById(id).get();
    }

    public void delete(int id){
        empRepo.deleteById(id);
    }

    public void exportReport(String reportFormat) throws FileNotFoundException, JRException{
        String pathFilePdf = "File/Report.pdf";
        String pathFileXls = "File/Report.xlsx";

        List<employee> employeeList = listTampil();

        File file = ResourceUtils.getFile("classpath:templates/Report.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(employeeList);
        Map<String, Object> params = new HashMap<>();
        params.put("Created By", "M. Ikhsan Gumanof");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
        if(reportFormat.equalsIgnoreCase("pdf")){
            JasperExportManager.exportReportToPdfFile(jasperPrint, pathFilePdf);
        }
        else if(reportFormat.equalsIgnoreCase("xls")){
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pathFileXls));
            exporter.exportReport();
        }
    }
}
