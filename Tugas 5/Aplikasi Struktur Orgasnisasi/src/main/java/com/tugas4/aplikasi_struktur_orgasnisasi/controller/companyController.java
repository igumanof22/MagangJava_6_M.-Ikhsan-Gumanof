package com.tugas4.aplikasi_struktur_orgasnisasi.controller;

import com.tugas4.aplikasi_struktur_orgasnisasi.model.company;
import com.tugas4.aplikasi_struktur_orgasnisasi.service.companyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class companyController {
    @Autowired
    private companyService compService;

    @RequestMapping("/company")
    public String viewHomePageCompany(Model model){
        List<company> listCompany = compService.listAll();
        model.addAttribute("listCompany", listCompany);
        return "company";
    }

    @RequestMapping("/company/new")
    public String viewNewPageCompany(Model model){
        company comp = new company();
        model.addAttribute(comp);
        return ("newCompany");
    }

    @RequestMapping(value = "/saveCompany", method = RequestMethod.POST)
    public String saveCompany(@ModelAttribute("company") company model){
        compService.save(model);
        return ("redirect:/company");
    }

    @RequestMapping("/company/edit/{id}")
    public ModelAndView viewEditPageEmployee(@PathVariable(name = "id") int id){
        ModelAndView mav = new ModelAndView("editCompany");
        company comp = compService.get(id);
        mav.addObject("comp", comp);
        return mav;
    }

    @RequestMapping("/company/delete/{id}")
    public String deleteCompany(@PathVariable(name = "id") int id){
        compService.delete(id);
        return "redirect:/company";
    }
}
