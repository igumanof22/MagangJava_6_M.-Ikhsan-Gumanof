package com.tugas4.aplikasi_struktur_orgasnisasi.controller;

import com.tugas4.aplikasi_struktur_orgasnisasi.model.employee;
import com.tugas4.aplikasi_struktur_orgasnisasi.service.employeeService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.io.FileNotFoundException;
import java.util.List;

@Controller
public class employeeController {
    @Autowired
    private employeeService empService;

    @RequestMapping("/employee")
    public String viewHomePageEmployee(Model model){
        List<employee> listEmployee = empService.listTampil();
        model.addAttribute("listEmployee", listEmployee);

        return "employee";
    }

    @RequestMapping("/employee/new")
    public String viewNewPageEmployee(Model model){
        employee emp = new employee();
        model.addAttribute(emp);
        return "newEmployee";
    }

    @RequestMapping(value = "/saveEmployee", method = RequestMethod.POST)
    public String saveEmployee(@ModelAttribute("employee") employee model){
        empService.save(model);
        return ("redirect:/employee");
    }

    @RequestMapping("/employee/edit/{id}")
    public ModelAndView viewEditEmployee(@PathVariable(name = "id") int id){
        ModelAndView mav = new ModelAndView("editEmployee");
        employee emp = empService.get(id);
        mav.addObject("emp", emp);
        return mav;
    }

    @RequestMapping("/employee/delete/{id}")
    public String deleteEmployee(@PathVariable(name = "id") int id){
        empService.delete(id);
        return "redirect:/employee";
    }

    @RequestMapping("/createReportPdf")
    public String ReportsPdf() throws FileNotFoundException, JRException{
        empService.exportReport("pdf");
        return "redirect:/employee";
    }

    @RequestMapping("/createReportXls")
    public String ReportsXls() throws FileNotFoundException, JRException{
        empService.exportReport("xls");
        return "redirect:/employee";
    }
}
