package com.tugas2.helloworld;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class WebController {
    @RequestMapping("/hello")
    public String sayHello(Model model){
        System.out.println("Saying hello world Spring Boot....");
        model.addAttribute("message", "Greeting!!!");
        return "hello";
    }
}
