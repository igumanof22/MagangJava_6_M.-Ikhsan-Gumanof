package com.tugas2.genapganjil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "com.tugas2.genapganjil" })
public class GenapganjilApplication {

	public static void main(String[] args) {
		SpringApplication.run(GenapganjilApplication.class, args);
	}

}
