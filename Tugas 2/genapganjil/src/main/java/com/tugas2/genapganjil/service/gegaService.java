package com.tugas2.genapganjil.service;

import com.tugas2.genapganjil.model.gegaModel;
import org.springframework.stereotype.Service;

@Service
public class gegaService {
    public String[] gega(gegaModel ggModel){
        int x = 0;
        int a = ggModel.getA();
        int b = ggModel.getB();
        int c = b-a;
        String[] out = new String[c+1];
        if(a<=b){
            for(int i=a; i<=b; i++){
                if(i%2==0){
                    out[x] = "Angka "+i+" adalah genap";
                    x++;
                }else{
                    out[x] = "Angka "+i+" adalah ganjil";
                    x++;
                }
            }
        } else {
            out[x] = "Angka Kedua harus lebih besar dari Angka Pertama";
            x++;
        }
        return out;
    }
}
