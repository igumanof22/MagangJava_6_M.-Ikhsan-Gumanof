package com.tugas2.genapganjil.model;

public class gegaModel {
    private int a;
    private int b;

    public gegaModel() {
    }

    public gegaModel(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}
