package com.tugas2.genapganjil.controller;

import com.tugas2.genapganjil.model.gegaModel;
import com.tugas2.genapganjil.service.gegaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class gegaController {
    gegaModel ggModel = new gegaModel();

    @Autowired
    private gegaService ggService;

    @RequestMapping("/")
    public String getGenapGanjil(Model model){
        model.addAttribute("ggModel", ggModel);
        return "index";
    }

    @RequestMapping(value = "/", params = "cari", method = RequestMethod.POST)
    public String cari(@ModelAttribute("ggModel") gegaModel ggModel, Model model){
        model.addAttribute("hasil", ggService.gega(ggModel));
        return "index";
    }
}
