package com.tugas2.kalkulator.service;

import com.tugas2.kalkulator.model.operasiModel;
import org.springframework.stereotype.Service;

@Service
public class operasiService {
    public int tambah(operasiModel opModel){
        return opModel.getA() + opModel.getB();
    }

    public int kurang(operasiModel opModel){
        return opModel.getA() - opModel.getB();
    }

    public int kali(operasiModel opModel){
        return opModel.getA() * opModel.getB();
    }

    public String bagi(operasiModel opModel){
        String hasil;
        String a = String.valueOf(opModel.getA());
        String b = String.valueOf(opModel.getB());

        if (opModel.getB()==0){
            hasil = "Tidak bisa dilakukan";
        } else{
            double x = Double.parseDouble(a) / Double.parseDouble(b);
            hasil = String.valueOf(x);
        }
        return hasil;
    }
}
