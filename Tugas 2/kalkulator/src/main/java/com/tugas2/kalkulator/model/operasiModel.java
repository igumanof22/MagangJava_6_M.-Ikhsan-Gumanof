package com.tugas2.kalkulator.model;

public class operasiModel {
    private String operasi;
    private int a;
    private int b;

    public operasiModel() {
    }

    public operasiModel(String operasi) {
        this.operasi = operasi;
    }

    public operasiModel(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public String getOperasi() {
        return operasi;
    }

    public void setOperasi(String operasi) {
        this.operasi = operasi;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }
}
