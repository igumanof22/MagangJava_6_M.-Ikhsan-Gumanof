package com.tugas2.kalkulator.controller;

import com.tugas2.kalkulator.model.operasiModel;
import com.tugas2.kalkulator.service.operasiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class operasiController {
    operasiModel opModel = new operasiModel();

    @Autowired
    private operasiService opService;

    @RequestMapping("/")
    public String getKalkulator(Model model){
        model.addAttribute("opModel", opModel);
        return "index";
    }

    @RequestMapping(value = "/", params = "tambah", method = RequestMethod.POST)
    public String tambah(@ModelAttribute("opModel") operasiModel opModel, Model model){
        model.addAttribute("hasil", opService.tambah(opModel));
        return "index";
    }

    @RequestMapping(value = "/", params = "kurang", method = RequestMethod.POST)
    public String kurang(@ModelAttribute("opModel") operasiModel opModel, Model model){
        model.addAttribute("hasil", opService.kurang(opModel));
        return "index";
    }

    @RequestMapping(value = "/", params = "kali", method = RequestMethod.POST)
    public String kali(@ModelAttribute("opModel") operasiModel opModel, Model model){
        model.addAttribute("hasil", opService.kali(opModel));
        return "index";
    }

    @RequestMapping(value = "/", params = "bagi", method = RequestMethod.POST)
    public String bagi(@ModelAttribute("opModel") operasiModel opModel, Model model){
        model.addAttribute("hasil", opService.bagi(opModel));
        return "index";
    }
}
